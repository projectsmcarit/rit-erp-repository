FROM python:3.6
ENV PYTHONUNBUFFERED 1
#RUN mkdir -p /my_projects/mysite/
ADD my_projects /
WORKDIR /my_projects/mysite
ADD requirements.txt /my_projects/mysite
RUN pip install --upgrade pip && pip install -r requirements.txt

