from django.shortcuts import render, redirect, get_object_or_404

from .models import AcademicYear, Courses
from .forms import AYForm, CourseForm


# Create your views here.

def displayAY(request):
	ay=AcademicYear.objects.all() # Collect all records from table 
	return render(request,'displayAY.html',{'ay':ay})

def createAY(request):
    if request.method == 'POST':
        form = AYForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('displayAY')
    form = AYForm()

    return render(request,'create.html',{'form': form})


def displayCourse(request):
	cs=Courses.objects.all() # Collect all records from table 
	return render(request,'displayCourse.html',{'cs':cs})

def createCourse(request):
    if request.method == 'POST':
        form = CourseForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('displayCourse')
    form = CourseForm()

    return render(request,'create.html',{'form': form})

