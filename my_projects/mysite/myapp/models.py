# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AcademicYear(models.Model):
    year_id = models.AutoField(primary_key=True)
    acd_year = models.TextField()
    status = models.IntegerField()

    class Meta:
        #managed = False
        db_table = 'academic_year'


class AdmissionStatus(models.Model):
    id = models.IntegerField(primary_key=True)
    course = models.CharField(max_length=2, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'admission_status'


class Courses(models.Model):
    id = models.CharField(primary_key=True, max_length=5)
    course = models.CharField(max_length=20)
    category = models.CharField(max_length=20)
    no_of_semesters = models.IntegerField()

    class Meta:
        #managed = False
        db_table = 'courses'



