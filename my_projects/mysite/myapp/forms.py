from django import forms
from .models import AcademicYear, Courses  
 
class AYForm(forms.ModelForm):
    class Meta:
        model = AcademicYear
        fields = "__all__"

class CourseForm(forms.ModelForm):
    class Meta:
        model = Courses
        fields = "__all__"

